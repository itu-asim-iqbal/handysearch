package com.example.raafs.hci_finalproject;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class StartJobs extends AppCompatActivity {


    String AssumedPhoneNo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_jobs);


        Button start = findViewById(R.id.start);
        Button stop = findViewById(R.id.stop);

        Intent intent = getIntent();

        AssumedPhoneNo = intent.getStringExtra("HandyNumber");











        FirebaseAuth mAuth = FirebaseAuth.getInstance();

        mAuth.signInAnonymously().addOnSuccessListener(this, new OnSuccessListener<AuthResult>() {
            @Override public void onSuccess(AuthResult authResult) {
                // do your stuff
            }
        }) .addOnFailureListener( this, new OnFailureListener() {
            @Override public void onFailure(@NonNull Exception exception) {
                Log.e("TAG", "signInAnonymously:FAILURE", exception);
            }
        });





        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent = new Intent(StartJobs.this,HandyRequests.class);
                intent.putExtra("HandyManNumber",AssumedPhoneNo);
                startService(intent);

            }
        });

        stop.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                stopService( new Intent ( StartJobs.this , HandyRequests.class ) ) ;
            }
        });







    }
}
