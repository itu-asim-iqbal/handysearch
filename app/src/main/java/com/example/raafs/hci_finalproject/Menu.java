package com.example.raafs.hci_finalproject;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Menu extends ListActivity {


    ListView listView;
    static ArrayAdapter<String> adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        listView = (ListView) findViewById(android.R.id.list);

        String[] items = { "Home", "My Profile", "My Orders",
                "About", "Share", "Rate Us", "LogOut"};
        adapter = new ArrayAdapter<String>(getListView().getContext(), android.R.layout.simple_list_item_1, items);

        getListView().setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String text = listView.getItemAtPosition(position).toString();
                if(text == "Home")
                {
                    Intent intent = new Intent(Menu.this, HomeScreen.class);
                    startActivity(intent);
                } else if(text == "My Orders")
                {
                    Intent intent = new Intent(Menu.this, MyOrders.class);
                    startActivity(intent);
                } else if(text == "LogOut")
                {
                    Intent intent = new Intent(Menu.this, LogIn.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });

/*
        liste = new ArrayList<String>();
        Collections.addAll(liste, values);
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, liste);
        setListAdapter(adapter);*/
    }


}
