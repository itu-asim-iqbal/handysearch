package com.example.raafs.hci_finalproject;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class HandyButton extends RelativeLayout
{
    public int handyID ;
    public HandyButton (Context context , int id )
    {
        super (context );
        if ( ! ( context instanceof Activity) ) return;

        View v = ( (Activity)context).findViewById(id);
        if ( ! ( v instanceof RelativeLayout ) ) return ;
        RelativeLayout layout = (RelativeLayout) v ;
        ViewGroup.LayoutParams params = layout.getLayoutParams();
        this.setLayoutParams (params);
        Button bt = new Button(context);
        this.setBackgroundDrawable(bt.getBackground());

        // copy all child from relative layout to this button
        while (layout.getChildCount() > 0)
        {
            View vchild = layout.getChildAt(0);
            layout.removeViewAt(0);
            this.addView(vchild);

            // if child is textView set its color to standard buttong text colors
            // using temporary instance of Button class
            if (vchild instanceof TextView)
            {
                ((TextView)vchild).setTextColor(bt.getTextColors());
            }

            // just to be sure that child views can't be clicked and focused
            vchild.setClickable(false);
            vchild.setFocusable(false);
            vchild.setFocusableInTouchMode(false);
        }

        // remove all view from layout (maybe it's not necessary)
        layout.removeAllViews();

        // set that this button is clickable, focusable, ...
        this.setClickable(true);
        this.setFocusable(true);
        this.setFocusableInTouchMode(false);

        // replace relative layout in parent with this one modified to looks like button
        ViewGroup vp = (ViewGroup)layout.getParent();
        int index = vp.indexOfChild(layout);
        vp.removeView(layout);
        vp.addView(this,index);

        this.setId(id);
    }



    public void setText ( int id , CharSequence text )
    {
        View v = findViewById ( id );
        if ( null != v && v instanceof TextView )
        {
            ( (TextView) v).setText(text);
        }
    }

    public void setImageDrawable ( int id, Drawable drawable )
    {
        View v = findViewById(id);
        if (null != v && v instanceof ImageView)
        {
            ((ImageView)v).setImageDrawable(drawable);
        }
    }

    public void setImageResource(int id, int image_resource_id)
    {

        View v = findViewById(id);
        if (null != v && v instanceof ImageView)
        {
            ((ImageView)v).setImageResource(image_resource_id);
        }
    }

    public void setRating(int id, float rating)
    {

        View v = findViewById(id);
        if (null != v && v instanceof ImageView)
        {
            ((RatingBar)v).setRating(rating);
        }

    }

    public void setHandyID ( int hID )
    {
        handyID = hID ;
    }
}
