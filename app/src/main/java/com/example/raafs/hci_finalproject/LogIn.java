package com.example.raafs.hci_finalproject;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LogIn extends AppCompatActivity {

    private String phone ;
    private String password ;
    private boolean isConsumer ;

    EditText etPhone ;
    EditText etPW ;
    Button btnSignIn ;
    Button btnRegister ;
    TextView tvRequire;

    private String fbPW;
    private SharedPreferences sp;

    private boolean goAhead = false ;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);

        etPhone = (EditText) findViewById(R.id.etPhone);
        etPW = (EditText) findViewById(R.id.etPW);
        btnSignIn = (Button) findViewById(R.id.btnSignIn);
        btnRegister = (Button) findViewById (R.id.btnRegister) ;
        tvRequire = (TextView) findViewById(R.id.tvRequire);

        sp = getApplicationContext().getSharedPreferences( "userDetails", MODE_PRIVATE ) ;
        phone = sp.getString ( "phone" , null ) ;
        etPhone.setText ( phone ) ;
        btnSignIn.setOnClickListener(new View.OnClickListener()
         {
             @Override
             public void onClick(View v)
             {
                 signIn ( );
             }
         });
        btnRegister.setOnClickListener(new View.OnClickListener()
         {
             @Override
             public void onClick(View v)
             {
                 askUser();
                 new Thread(new Runnable()
                 {
                     @Override
                     public void run()
                     {
                         while ( !goAhead ) android.os.SystemClock.sleep ( 200 ) ;
                         Intent i = new Intent ( LogIn.this, Registration.class ) ;
                         Bundle b = new Bundle();
                         if ( isConsumer )  b.putString ("userType" , "CONSUMER" ) ;
                         else               b.putString ("userType" , "REPAIRPERSON" ) ;
                         i.putExtras(b);
                         i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                         startActivity(i);
                     }
                 }).start();

             }
         });
    }

    public void signIn ( )
    {
        phone = etPhone.getText().toString();
        askUser ( ) ;
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                while ( !goAhead ) android.os.SystemClock.sleep ( 200 ) ;
                DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
                DatabaseReference userNameRef ;
                if ( isConsumer )
                {
                    userNameRef = rootRef.child("Users").child(phone);
                }
                else
                {
                    userNameRef = rootRef.child("Handyman").child(phone);
                }
                ValueEventListener eventListener = new ValueEventListener()
                {

                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        if ( dataSnapshot.exists() )
                        {
                            fbPW = dataSnapshot.child ( "Password" ).getValue(String.class);
                            password = etPW.getText().toString();
                            if ( fbPW.equals ( password ) )
                            {
                                sp.edit().putString ( "name" , dataSnapshot.child ( "Name" ).getValue(String.class) ).apply();
                                sp.edit().putString ( "email" , dataSnapshot.child ( "Email" ).getValue(String.class) ).apply();
                                sp.edit().putString ( "phone" , phone ).apply();

                                Intent i ;
                                if ( isConsumer ) i = new Intent ( LogIn.this , HomeScreen.class);
                                else
                                {
                                    i = new Intent ( LogIn.this , StartJobs.class);
                                    i.putExtra("HandyNumber",phone);
                                }

                                startActivity(i);
                                finish();
                            }
                            else
                            {
                                tvRequire.setText("Invalid password. Please try again");
                            }
                        }
                        else
                        {
                            tvRequire.setText("Username not found");
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                };
                userNameRef.addListenerForSingleValueEvent(eventListener);
            }
        }).start();

    }

    private void askUser ( )
    {
        goAhead = false;
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                LayoutInflater li = LayoutInflater.from(LogIn.this);
                View promptsView = li.inflate ( R.layout.ask_user, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder ( LogIn.this ) ;
                alertDialogBuilder.setView ( promptsView ) ;

                final Button btnUser = (Button) promptsView.findViewById(R.id.btnUser);
                final Button btnHandyman = (Button) promptsView.findViewById(R.id.btnHandyman);

                alertDialogBuilder.setCancelable(false) ;
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                btnUser.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        isConsumer = true ;
                        SharedPreferences userDetails = getApplicationContext().getSharedPreferences( "userDetails", MODE_PRIVATE ) ;
                        SharedPreferences.Editor ed = userDetails.edit();
                        ed.putBoolean ( "initialized" , true ) ;
                        ed.putBoolean ( "isConsumer" , isConsumer ) ;
                        ed.apply();
                        alertDialog.dismiss();
                        goAhead = true ;
                    }
                });

                btnHandyman.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        isConsumer = false ;
                        SharedPreferences userDetails = getApplicationContext().getSharedPreferences( "userDetails", MODE_PRIVATE ) ;
                        SharedPreferences.Editor ed = userDetails.edit();
                        ed.putBoolean ( "initialized" , true ) ;
                        ed.putBoolean ( "isConsumer" , isConsumer ) ;
                        ed.apply();
                        alertDialog.dismiss();
                        goAhead = true ;
                    }
                });
            }
        });
    }
}


