package com.example.raafs.hci_finalproject;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;


// In this activity, we will check if there is login information saved in the app and take the user to the login screen.
// If no login information is saved, the user will be taken to the registration screen.

public class SplashActivity extends AppCompatActivity
{

    Intent intent ;
    Bundle b;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        //PERMISSIONS
        int PERMISSION_ALL = 1;
        String[] PERMISSIONS = {
                Manifest.permission.INTERNET,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.CALL_PHONE,
                Manifest.permission.READ_PHONE_STATE,
                Manifest.permission.PROCESS_OUTGOING_CALLS

        };

        if (Build.VERSION.SDK_INT > 22)
        {
            if (!hasPermissions(this, PERMISSIONS))
            {
                ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
            }
        }

        b = new Bundle ();

        final TextView tvalready = (TextView) findViewById(R.id.tvalready);
        tvalready.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                intent = new Intent ( SplashActivity.this , LogIn.class ) ;
                b.putString ( "userName" , null ) ;
                intent.putExtras(b);
                startActivity ( intent ) ;
                finish();
            }
        });
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                android.os.SystemClock.sleep(1000);
                SharedPreferences userDetails = getApplicationContext().getSharedPreferences( "userDetails", MODE_PRIVATE ) ;
                SharedPreferences.Editor ed;
                if ( !userDetails.contains("initialized") )
                {
                    final LinearLayout chooserlayout = (LinearLayout) findViewById(R.id.chooserlayout);
                    final TextView select = (TextView) findViewById(R.id.select);

                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            chooserlayout.setVisibility(LinearLayout.VISIBLE);
                            select.setVisibility(TextView.VISIBLE);
                            tvalready.setVisibility(TextView.VISIBLE);
                        }
                    });

                    Button consumer = (Button) findViewById(R.id.consumer);
                    consumer.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            userRegBtn();
                        }
                    });

                    Button repairperson = (Button) findViewById(R.id.repairperson);
                    repairperson.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View v)
                        {
                            handyRegBtn();
                        }
                    });

                }

                else
                {
                    String userName = userDetails.getString ( "userName", "" ) ;
                    intent = new Intent ( SplashActivity.this , LogIn.class );
                    b.putString ( "userName" , userName ) ;
                    intent.putExtras(b);
                    startActivity ( intent ) ;
                    finish();
                }
            }
        }).start();



    }


    public void userRegBtn ( )
    {
        intent = new Intent(SplashActivity.this, Registration.class);
        b.putString ("userType" , "CONSUMER" ) ;
        intent.putExtras(b);
        startActivity(intent);
        finish();
    }

    public void handyRegBtn ( )
    {
        intent = new Intent(SplashActivity.this, Registration.class);
        b.putString ("userType" , "REPAIRPERSON" ) ;
        intent.putExtras(b);
        startActivity(intent);
        finish();
    }

    public static boolean hasPermissions(Context context, String... permissions)
    {
        if (context != null && permissions != null)
        {
            for (String permission : permissions)
            {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED)
                {
                    return false;
                }
            }
        }
        return true;
    }

}
