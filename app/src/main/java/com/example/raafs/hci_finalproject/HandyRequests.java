package com.example.raafs.hci_finalproject;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FileDownloadTask;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by safi on 5/21/18.
 */

public class HandyRequests extends Service {


    String AssumedPhoneNo;

    FirebaseStorage storage = FirebaseStorage.getInstance();






    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    @Override
    public int onStartCommand(final Intent intent, int flags, int startId) {
        // Let it continue running until it is stopped.


        Toast.makeText(this, "Service Started", Toast.LENGTH_LONG).show();


        AssumedPhoneNo= intent.getStringExtra("HandyManNumber");

        Log.d("TAG", "phonenumber: " + AssumedPhoneNo);



        final Handler handler = new Handler();
        Timer timer = new Timer();


        TimerTask doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        // Add your code here.


                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference();

                        DatabaseReference myRef2;









                        myRef2=myRef.child("Handyman").child(AssumedPhoneNo).child("Orders");







                        myRef2.addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {

                                HashMap<?, ?> value= (HashMap<?,?>) dataSnapshot.getValue();
                                Iterator myVeryOwnIterator = value.keySet().iterator();
                                while(myVeryOwnIterator.hasNext()) {
                                    String key=(String)myVeryOwnIterator.next();
                                    Log.d("TAG", "Value is: " + key);

                                    try {
                                        DownloadAudioFile(key,AssumedPhoneNo);






                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }


                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }





                        });


                    }

                });
            }
        };
        //Starts after 20 sec and will repeat on every 20 sec of time interval.
        timer.schedule(doAsynchronousTask, 20000,20000);  // 20 sec timer









        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
    }




    public Boolean DownloadAudioFile(final String UserPhoneNumber, final String HandyManPhoneNumber) throws IOException {


        final Boolean[] Success = {false};

        // Create a reference with an initial file path and name
        StorageReference storageRef = storage.getReference();


        StorageReference pathReference = storageRef.child("Audio/"+HandyManPhoneNumber+"/"+UserPhoneNumber+".mp3");


        final File file = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MUSIC), UserPhoneNumber+".mp3");

        Toast.makeText(getApplicationContext()," Downloaded: "+ file.getAbsolutePath(),Toast.LENGTH_LONG);


        pathReference.getFile(file)
                .addOnSuccessListener(new OnSuccessListener<FileDownloadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(FileDownloadTask.TaskSnapshot taskSnapshot) {


                    Success[0] =true;

                        Toast.makeText(getApplicationContext(),"File Downloaded: ",Toast.LENGTH_LONG);


                        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getApplicationContext());



                        mBuilder.setSmallIcon(R.drawable.ac);
                        mBuilder.setContentTitle("ہینڈی سرچ سے اہم اعلان");
                        mBuilder.setContentText("آپ کے لئے ایک نیا کام ہے");

                        Intent resultIntent = new Intent(getApplicationContext(), HandyManEnd.class);
                        resultIntent.putExtra("UserPhoneNumber",UserPhoneNumber);
                        resultIntent.putExtra("HandyManNumber",HandyManPhoneNumber);

                        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);


                        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getApplicationContext());
                        stackBuilder.addParentStack(HandyManEnd.class);

                        // Adds the Intent that starts the Activity to the top of the stack
                        stackBuilder.addNextIntent(resultIntent);
                        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,PendingIntent.FLAG_UPDATE_CURRENT);
                        mBuilder.setContentIntent(resultPendingIntent);

                        NotificationManager mNotificationManager = (NotificationManager) getSystemService(getApplicationContext().NOTIFICATION_SERVICE);

                        // notificationID allows you to update the notification later on.
                        mNotificationManager.notify(0, mBuilder.build());






                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {

                Toast.makeText(getApplicationContext(),"Something went wrong",Toast.LENGTH_LONG);

                // Handle failed download
                // ...
            }
        });







        return Success[0];


    }






}
