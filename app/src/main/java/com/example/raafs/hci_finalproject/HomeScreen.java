package com.example.raafs.hci_finalproject;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class HomeScreen extends AppCompatActivity {

    public static final String FIREBASE_URL = "https://hcifinalproject-626d0.firebaseio.com/";
    private SharedPreferences sp ;
    private String uName;
    private String uEmail;
    private String uPhone;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference myRef = database.getReference("Handyman");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        sp = getApplicationContext().getSharedPreferences( "userDetails", MODE_PRIVATE ) ;
        uName = sp.getString ( "name" , "" ) ;
        uEmail = sp.getString ( "email" , "" ) ;
        uPhone = sp.getString ( "phone" , "" ) ;


        myRef.child("03071100604").child("Name").setValue("Shams Alam");
        myRef.child("03071100604").child("Rating").setValue("4.0");
        myRef.child("03071100604").child("Location").setValue("Illama Iqbal Town");
        myRef.child("03071100604").child("Tag").setValue("Electrician");
        TextView tvHomeTitle = (TextView) findViewById(R.id.tvHomeTitle);
        tvHomeTitle.setText ( uName ) ;

        ImageView electrician, plumber, carpenter, ac, menu;
        menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (HomeScreen.this, Menu.class);
                startActivity(intent);
            }
        });




        electrician = (ImageView)findViewById(R.id.electrician);
        electrician.setOnClickListener(new View.OnClickListener() {
         @Override
            public void onClick(View v) {
                                                String strName = "Electricians";
                                                Intent intent = new Intent(HomeScreen.this, HandyMen.class);
                                                intent.putExtra("HandyMan", strName);
                                                startActivity(intent);

        }
        }
        );
        plumber = (ImageView)findViewById(R.id.plumber);
        plumber.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               String strName = "Plumbers";
                                               Intent intent = new Intent(HomeScreen.this, HandyMen.class);
                                               intent.putExtra("HandyMan", strName);
                                               startActivity(intent);

                                           }
                                       }
        );

        carpenter = (ImageView)findViewById(R.id.carpenter);
        carpenter.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               String strName = "Carpenters";
                                               Intent intent = new Intent(HomeScreen.this, HandyMen.class);
                                               intent.putExtra("HandyMan", strName);
                                               startActivity(intent);

                                           }
                                       }
        );

        ac = (ImageView)findViewById(R.id.ac);
        ac.setOnClickListener(new View.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               String strName = "AC Technicians";
                                               Intent intent = new Intent(HomeScreen.this, HandyMen.class);
                                               intent.putExtra("HandyMan", strName);
                                               startActivity(intent);

                                           }
                                       }
        );






    }
}
