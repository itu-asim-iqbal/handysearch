package com.example.raafs.hci_finalproject;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Registration extends AppCompatActivity {


  //  final FirebaseDatabase database = FirebaseDatabase.getInstance();
    //DatabaseReference ref = database.getReference("server/saving-data/fireblog");

    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    private String userType;

    private String uName;
    private String uEmail;
    private String uPW;
    private String uCPW;
    private String uPhoneNum;
    private String uAddress;
    private String uTag ;

    private boolean allClear = false ;
    private boolean isConsumer = false;


    EditText name, phone, email, address, password, confirmpassword;
    Button submit;
    TextView require, tvalready, tvTag;
    RadioGroup rgProfession;

    private String emailCode ;
    private String phoneCode;
    private boolean stepComplete = false ;

    private boolean verifyPhone = false ;
    private boolean verifyUser = false ;

    private int passwordMinLength = 4;


    boolean proceedWithCheck = false ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);



        emailCode = "" + (new Random().nextInt(10000 - 1000) + 1000) ;
        phoneCode = "" + (new Random().nextInt(10000 - 1000) + 1000) ;

        mDatabase = FirebaseDatabase.getInstance().getReference();

        name = (EditText) findViewById(R.id.Name);
        password = (EditText) findViewById(R.id.password);
        confirmpassword = (EditText) findViewById(R.id.confirmpassword);
        address = (EditText) findViewById(R.id.address);
        phone = (EditText) findViewById(R.id.phoneNo);
        email = (EditText) findViewById(R.id.email);
        submit = (Button) findViewById(R.id.registration);
        require = (TextView) findViewById(R.id.require);
        ImageView imageView3 = (ImageView) findViewById(R.id.imageView3);
        password.setHint ("Password (Must be " + passwordMinLength + " characters long") ;

        tvTag = (TextView) findViewById(R.id.tvTag);
        rgProfession = (RadioGroup) findViewById(R.id.rgProfession);
        ImageView imageView7 = (ImageView) findViewById(R.id.imageView7);

        tvalready = (TextView) findViewById(R.id.tvalready);

        userType = getIntent().getStringExtra ("userType");
        if (userType.equalsIgnoreCase("CONSUMER"))
        {
            isConsumer = true;
//            Toast.makeText(Registration.this , "isConsumer = " + isConsumer , Toast.LENGTH_SHORT).show();
        }
        else
        {
//            Toast.makeText(Registration.this , "isConsumer = " + isConsumer , Toast.LENGTH_SHORT).show();
            isConsumer = false;
            email.setVisibility(EditText.GONE);
            imageView3.setVisibility(ImageView.GONE);
            tvTag.setVisibility(TextView.VISIBLE);
            rgProfession.setVisibility(RadioGroup.VISIBLE);
            imageView7.setVisibility(ImageView.VISIBLE);
        }

        tvalready.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent ( Registration.this , LogIn.class ) ;
                Bundle b = new Bundle ();
                b.putString ( "userName" , null ) ;
                intent.putExtras(b);
                startActivity ( intent ) ;
                finish();
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                allClear = checkFields ( );
                if ( allClear )
                {
                    if ( isConsumer )
                    {
                        registerConsumer () ;
                    }
                    else
                    {
                        registerHandyPerson();
                    }
                }
            }
        });
    }

    private boolean checkFields ( )
    {
        allClear = false;

        uName = name.getText().toString() ;
        uEmail = email.getText().toString() ;
        uPW = password.getText().toString() ;
        uCPW = confirmpassword.getText().toString() ;
        uPhoneNum = phone.getText().toString() ;
        uAddress = address.getText().toString() ;
        uTag = "" ;
        if ( !isConsumer )
        {
            int professionSelection = rgProfession.getCheckedRadioButtonId();
            RadioButton rb = (RadioButton) findViewById(professionSelection);
            uTag = rb.getText().toString();
        }

        if (uName.isEmpty()) name.setHint("Name Required");
        if (uEmail.isEmpty() && isConsumer) email.setHint("Email Required");
        if (uPW.isEmpty()) password.setHint("Password Required");
        if (uCPW.isEmpty()) confirmpassword.setHint("Must Confirm Password");
        if (uPhoneNum.isEmpty()) phone.setHint("Phone Required");
        if ( uTag.isEmpty() && !isConsumer ) tvTag.setText ( "Please select a profession");

        if (    !uName.isEmpty() && ( ( isConsumer && !uEmail.isEmpty() ) || ( !isConsumer && !uTag.isEmpty()) ) &&
                                        !uPW.isEmpty() &&
                                        !uCPW.isEmpty() &&
                                        !uPhoneNum.isEmpty() )
        {
            if ( uPW.length() < passwordMinLength )
            {
                password.setText ("");
                confirmpassword.setText ("");
                password.setHint("Password must be minimum " + passwordMinLength + " characters") ;

            }
            else if (!(uPW.equals(uCPW)))
            {
                password.setText ("");
                confirmpassword.setText ("");
                confirmpassword.setHint("Password does not match") ;
            }
            else allClear = true ;
        }
        return allClear;
    }

    public void registerConsumer ()
    {
        //Check if email is valid
        final String username = "mohammad.asim.iqbal.cs@gmail.com" ;
        final String password = "polkadots28" ;
        final String subject = "Email Verification Code for HandySearch" ;
        final String body = emailCode ;
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                try
                {
//                    if ( true )
                    if ( checkUniqueUser() )
                    {

                        //Verify Phone
//                        if ( verifyPhone )
//                        {
//                            //RUN CODE TO VERIFY PHONE NUMBER
//                        }
//                        //VERIFY EMAIL ADDRESS
//                        GMailSender sender = new GMailSender( username , password );
//                        sender.sendMail(subject, body, username, uEmail);
//                        String dMessage = "An email with a security code has been sent to " + uEmail + "\n Please enter the code below to verify your email address" ;
//                        showDialog ( dMessage ) ;
//                        while ( !stepComplete ) android.os.SystemClock.sleep ( 1000 ) ;
//                        stepComplete = false ;

                        UserData ud = new UserData( uName , uEmail , uPW , uPhoneNum , uAddress ) ;
                        mDatabase.child ("Users").child (uPhoneNum).setValue ( ud ) ;
                        SharedPreferences userDetails = getApplicationContext().getSharedPreferences( "userDetails", MODE_PRIVATE ) ;
                        SharedPreferences.Editor ed = userDetails.edit();
                        ed.putBoolean ( "initialized" , true ) ;
//                        ed.putBoolean ( "isConsumer" , isConsumer ) ;
//                        ed.putString ( "name" , uName ) ;
//                        ed.putString ( "email" , uEmail ) ;
//                        ed.putString ( "password" , uPW ) ;
                        ed.putString ( "phoneNum" , uPhoneNum ) ;
//                        ed.putString ( "address" , uAddress) ;
                        ed.apply();

                        Intent i = new Intent ( Registration.this , LogIn.class);
                        startActivity(i);
                        finish();
                    }
                    else
                    {
                        require.setText ( "An account with this phone number already exists\nPlease login using your credentials or enter a different phone number");
                    }
                }
                catch (Exception e)
                {
                    Log.e ( "REG" , e.toString() ) ;
                }
            }
        }).start();
    }

    public void registerHandyPerson()
    {
        new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                if ( checkUniqueUser() )
//                if(true)
                {
                    if ( verifyPhone )
                    {

                    }
                    float f = 0.0f;
                    HandymanData hd = new HandymanData(uName, ("" + f), uPW, uPhoneNum, uAddress, uTag);
                    mDatabase.child("Handyman").child(uPhoneNum).setValue(hd);
                    SharedPreferences userDetails = getApplicationContext().getSharedPreferences( "userDetails", MODE_PRIVATE ) ;
                    SharedPreferences.Editor ed = userDetails.edit();
                    ed.putBoolean ( "initialized" , true ) ;
//                        ed.putBoolean ( "isConsumer" , isConsumer ) ;
//                        ed.putString ( "name" , uName ) ;
//                        ed.putString ( "email" , uEmail ) ;
//                        ed.putString ( "password" , uPW ) ;
                    ed.putString ( "phoneNum" , uPhoneNum ) ;
//                        ed.putString ( "address" , uAddress) ;
                    ed.apply();

                    Intent i = new Intent ( Registration.this , LogIn.class);
                    startActivity(i);
                    finish();
                }
                else
                {
                    runOnUiThread(new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            require.setText ( "An account with this phone number already exists\nPlease login using your credentials or enter a different phone number");
                        }
                    });
                }
            }
        }).start();
    }

    private void showDialog (final String message )
    {
        runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                LayoutInflater li = LayoutInflater.from(Registration.this);
                View promptsView = li.inflate ( R.layout.custom_dialog, null);
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder ( Registration.this ) ;
                alertDialogBuilder.setView ( promptsView ) ;
                final TextView tvDialog = (TextView) promptsView.findViewById(R.id.tvDialog);
                final EditText etDialogUserInput = (EditText) promptsView.findViewById(R.id.etDialogUserInput);
                final Button btnSubmitDialog = (Button) promptsView.findViewById(R.id.btnSubmitDialog);

                tvDialog.setText ( message ) ;
                alertDialogBuilder.setCancelable(false) ;
                final AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();
                btnSubmitDialog.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        String userInput = etDialogUserInput.getText().toString();
                        if ( userInput.equals(emailCode) )
                        {
                            alertDialog.dismiss();
                            stepComplete = true ;
                        }
                        else
                        {
                            tvDialog.setText ( "Invalid code. Please recheck code and enter below" );
                        }
                    }
                });
            }
        });
    }

//    private boolean checkUniqueUser ( )
//    {
//        String node ;
//        if ( isConsumer ) node = "Users" ;
//        else node = "Handyman" ;
//        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference(node);
//        rootRef.addListenerForSingleValueEvent(new ValueEventListener()
//        {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
//                    if ((childDataSnapshot.getKey()).equals(uPhoneNum))
//                    {
//                        Log.i("Firebase", dataSnapshot.getValue().toString());
//                        runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                require.setText("UserData account already exists. Please return to the main screen and login using your credentials");
//                            }
//                        });
//                        verifyUser = false;
//                    } else verifyUser = true;
//                }
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError)
//            {
//
//            }
//        });
//        return verifyUser;
//    }

    private boolean checkUniqueUser ( )
    {
        String node ;
        if ( isConsumer ) node = "Users" ;
        else node = "Handyman" ;
        DatabaseReference rootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference userNameRef = rootRef.child(node).child(uPhoneNum);
        userNameRef.addListenerForSingleValueEvent(new ValueEventListener()
//        userNameRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if ( dataSnapshot.exists() )
                {
//                    Log.i("Firebase", dataSnapshot.getValue().toString());
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            require.setText("UserData account already exists. Please return to the main screen and login using your credentials");
//                        }
//                    });
                    verifyUser = false;
                }
                else verifyUser = true;
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
        return verifyUser;
    }
}
