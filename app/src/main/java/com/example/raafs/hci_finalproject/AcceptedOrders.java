package com.example.raafs.hci_finalproject;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.BufferedReader;

/**
 * Created by asim_ on 5/21/2018.
 */

public class AcceptedOrders extends Fragment
{
    FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
    DatabaseReference databaseReference =   mFirebaseDatabase.getReference().child("Handyman");
    DatabaseReference databaseReferenceHandyman = mFirebaseDatabase.getReference().child("Users");

    private LinearLayout mLinearLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_myorder_accepted, container, false);
        mLinearLayout = (LinearLayout) rootView.findViewById (R.id.accepted_linear_layout);

        SharedPreferences preferencesusers = rootView.getContext().getSharedPreferences("userDetails", Context.MODE_PRIVATE);
        final String user_number = preferencesusers.getString("phone", null);
        // rating = preferenceshandyman.getFloat("Rating", (float)0.0);
         //  final DatabaseReference databaseReferenceHandyman = mFirebaseDatabase.getReference().child("Handyman");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    Log.v("TAG 2",""+ childDataSnapshot.getKey()); //displays the key for the node
                    Log.v("TAG 1",""+ childDataSnapshot.child("Name").getValue());   //gives the value for given keyname
                    //String tag = String.valueOf(childDataSnapshot.child("Tag").getValue());

                    String status = String.valueOf(childDataSnapshot.child("Orders")
                            .child(user_number).child("Status").getValue());


                    if(status.equals("Accepted"))
                    {
                        Log.wtf("TAG Me",  String.valueOf(childDataSnapshot.child("Status").getValue()));
/*

                            Long phone = Long.parseLong(handy_number);*/
                        addLayout ( String.valueOf(childDataSnapshot.child("Name").getValue())
                                , String.valueOf(childDataSnapshot.child("Location").getValue()),
                                Float.parseFloat(String.valueOf(childDataSnapshot.child("Rating").getValue())),
                                Long.parseLong(String.valueOf(childDataSnapshot.getKey()) ) , rootView, user_number);



                    }

                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        /*addLayout ( "Shahzada Saleem" , "Mochi Gate" , 2.50f , 2 ) ;
        addLayout ( "Anarkali" , "Anarkali" , 5.0f , 3 ) ;
        addLayout ( "Badshah Akbar" , "Akbari Gate" , 1.50f , 1 ) ;
        addLayout ( "Shahzada Saleem" , "Mochi Gate" , 2.50f , 2 ) ;
        addLayout ( "Anarkali" , "Anarkali" , 5.0f , 3 ) ;
        addLayout ( "Badshah Akbar" , "Akbari Gate" , 1.50f , 1 ) ;
        addLayout ( "Shahzada Saleem" , "Mochi Gate" , 2.50f , 2 ) ;
        addLayout ( "Anarkali" , "Anarkali" , 5.0f , 3 ) ;
*/

        return rootView ;
    }


    private void addLayout ( String name , String location , float rating ,
                             final Long hndID, final View rootView, final String user_number)
    {
//        View hndBtn = LayoutInflater.from(this).inflate ( R.layout.handy_button, mLinearLayout, false );
        final View hndBtn = LayoutInflater.from(getContext()).inflate ( R.layout.tile_accepted, mLinearLayout, false );
        TextView tvName = (TextView) hndBtn.findViewById ( R.id.hbtn_Name);
        TextView tvLoc = (TextView) hndBtn.findViewById ( R.id.hbtn_Location);
        RatingBar rbRating = (RatingBar) hndBtn.findViewById ( R.id.hbtn_rating);
        hndBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                Toast.makeText(getApplicationContext(), "" + hndID , Toast.LENGTH_SHORT ).show();
                Toast.makeText(getContext(), "" + hndID , Toast.LENGTH_SHORT ).show();
            }
        });
        Button btnIDtoW = (Button) hndBtn.findViewById ( R.id.btnIDtoW);
        btnIDtoW.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
               // Log.wtf("Inside onClick", user_number);
                leaveFeedBackDialog(rootView, hndID, user_number);

            }
        });

        tvName.setText ( name );
        tvLoc.setText ( location ) ;
        rbRating.setRating ( rating ) ;

        mLinearLayout.addView ( hndBtn ) ;

    }
    public void leaveFeedBackDialog(View rootView, final Long number, final String user_number)
    {
        LayoutInflater li = LayoutInflater.from(rootView.getContext());
        View promptsView = li.inflate ( R.layout.leavefeedback_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder ( rootView.getContext() ) ;
        alertDialogBuilder.setView ( promptsView ) ;

        final RatingBar ratingBar = (RatingBar) promptsView.findViewById(R.id.ratingBar);
        TextView feedBack = (TextView) promptsView.findViewById(R.id.status);
        Button submit = (Button) promptsView.findViewById(R.id.feedback);

        alertDialogBuilder.setCancelable(false) ;
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Float rating = ratingBar.getRating();
                databaseReference.child("0"+number).child("Orders")
                        .child(user_number).child("Status").setValue("Completed");
                databaseReference.child("0"+number).child("Orders")
                        .child(user_number).child("Rating").setValue(""+ratingBar.getRating());
                databaseReferenceHandyman.child(user_number).child("Orders").child("0"+number)
                        .child("Status").setValue("Completed");

                alertDialog.dismiss();
                // "" + floatVar

            }
        });




    }
}
