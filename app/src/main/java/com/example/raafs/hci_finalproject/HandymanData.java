package com.example.raafs.hci_finalproject;

/**
 * Created by asim_ on 5/21/2018.
 */

public class HandymanData
{
    public String Name;
    public String Rating;
    public String Password;
    public String Phone;
    public String Location;
    public String Tag;


    public HandymanData ( String n, String r, String pw, String ph, String l, String t )
    {
        Name = n;
        Rating = r;
        Password = pw;
        Phone = ph;
        Location = l;
        Tag = t ;
    }
}
