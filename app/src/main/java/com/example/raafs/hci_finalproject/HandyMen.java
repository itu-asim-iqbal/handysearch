package com.example.raafs.hci_finalproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.provider.SyncStateContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.example.raafs.hci_finalproject.HomeScreen.FIREBASE_URL;
import static java.lang.Float.parseFloat;

public class HandyMen extends AppCompatActivity {
    private LinearLayout mLinearLayout;
    public int handymenCount;
    public List<String> handymenNames;
    public List<String> handymenLocations;
    public List<Float> handymenRatings;
    public List<Integer> handymenIDs;
    public  SharedPreferences sp;

   // private DatabaseReference mDatabase;

    public String name, location, rating;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handy_men);
     //   mDatabase = FirebaseDatabase.getInstance().getReference();


        TextView service;
        ImageButton handyman;
        ImageView menu;
        menu = (ImageView) findViewById(R.id.menu);
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (HandyMen.this, Menu.class);
                startActivity(intent);
            }
        });

        service = (TextView) findViewById(R.id.service);

        handyman = (ImageButton) findViewById(R.id.singleHandyman);
        Intent mIntent = getIntent();
        Bundle extras = mIntent.getExtras();
        final String data = extras.getString("HandyMan");
        service.setText(data);
        handyman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (HandyMen.this, HandymanInfo.class);
                startActivity(intent);
            }
        });

        mLinearLayout = (LinearLayout) findViewById(R.id.my_linear_layout);

        FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReference =    mFirebaseDatabase.getReference().child("Handyman");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {

                    Log.v("TAG 2",""+ childDataSnapshot.getKey()); //displays the key for the node
                    Log.v("TAG 1",""+ childDataSnapshot.child("Name").getValue());   //gives the value for given keyname
                    String tag = String.valueOf(childDataSnapshot.child("Tag").getValue());
                    tag = tag + "s";
                    Log.v("Equality", tag + " ==== " + data);
                    if(data.equals(tag))
                    {

                        String review = String.valueOf(childDataSnapshot.child("Rating").getValue());
                        String number = String.valueOf(childDataSnapshot.getKey());
                        Long phone = Long.parseLong(number);
                        Float rating = Float.parseFloat(review);
                        addLayout ( String.valueOf(childDataSnapshot.child("Name").getValue())
                                , String.valueOf(childDataSnapshot.child("Location").getValue())
                                , rating , phone , data);

                    }
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //}
           /*     try {
                    float rate = Float.valueOf(rating);
                    System.out.println("rateing is : "+rate);
                } catch (NumberFormatException e) {
                    System.out.println("numberStr is not a number");
                }*/



        //   ;
        /*addLayout ( "Shahzada Saleem" , "Mochi Gate" , 2.50f , 2 ) ;
        addLayout ( "Anarkali" , "Anarkali" , 5.0f , 3 ) ;
        addLayout ( "Badshah Akbar" , "Akbari Gate" , 1.50f , 1 ) ;
        addLayout ( "Shahzada Saleem" , "Mochi Gate" , 2.50f , 2 ) ;
        addLayout ( "Anarkali" , "Anarkali" , 5.0f , 3 ) ;
        addLayout ( "Badshah Akbar" , "Akbari Gate" , 1.50f , 1 ) ;
        addLayout ( "Shahzada Saleem" , "Mochi Gate" , 2.50f , 2 ) ;
        addLayout ( "Anarkali" , "Anarkali" , 5.0f , 3 ) ;
*/


    }

    private void addLayout (final String name , final String location , final float rating , final Long hndID, final String skill)
    {
        View hndBtn = LayoutInflater.from(this).inflate ( R.layout.handy_button, mLinearLayout, false );
        TextView tvName = (TextView) hndBtn.findViewById ( R.id.hbtn_Name);
        TextView tvLoc = (TextView) hndBtn.findViewById ( R.id.hbtn_Location);
        RatingBar rbRating = (RatingBar) hndBtn.findViewById ( R.id.hbtn_rating);
        final String number = String.valueOf(hndID);
//        final SharedPreferences.Editor editor = sp.edit();

        final String reviews = String.valueOf(rating);
        hndBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /**/
                SharedPreferences prefs = getApplicationContext().getSharedPreferences( "handymanDetails", MODE_PRIVATE ) ;;
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("Locaion", location);
                editor.putString("Name", name);
                editor.putFloat("Rating", rating);
                editor.putString("Skill", skill);
                editor.putString("Number", number);
                editor.commit();

                Intent intent = new Intent(HandyMen.this, HandymanInfo.class);
                intent.putExtra("Name", name);
                startActivity(intent);
                //Toast.makeText(getApplicationContext(), "" + hndID , Toast.LENGTH_SHORT ).show();
            }
        });

        tvName.setText ( name );
        tvLoc.setText ( location ) ;
        rbRating.setRating ( rating ) ;

        mLinearLayout.addView ( hndBtn ) ;

    }

}
