package com.example.raafs.hci_finalproject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import java.net.URL;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicMarkableReference;

public class AudioRecorder extends AppCompatActivity {
  //  final ViewTreeObserver mLayoutObserver = mLayout.getViewTreeObserver();
    private static MediaRecorder myAudioRecorder = new MediaRecorder();

    public String outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/job.3gp";
    private ProgressDialog progressDialog;
    private FirebaseStorage storage = FirebaseStorage.getInstance();
    private StorageReference storageRef = storage.getReference();
    private static final String LOG_TAG = " Record Log";
    private ImageView play, stop, send, record;
    private TextView label;
    public String PhoneNo;
    public String number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio_recorder);
        progressDialog = new ProgressDialog(this);
        final MediaPlayer mp = MediaPlayer.create(this, R.raw.handysearch);
        mp.start();

        FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReferencehandyman =    mFirebaseDatabase.getReference().child("Handyman");
        DatabaseReference databaseReferenceuser =    mFirebaseDatabase.getReference().child("Users");


        SharedPreferences preferencesuser, preferenceshandyman;
        preferencesuser = getApplicationContext().getSharedPreferences( "userDetails", MODE_PRIVATE ) ;
        preferenceshandyman = getApplicationContext().getSharedPreferences( "handymanDetails", MODE_PRIVATE ) ;
        PhoneNo = preferencesuser.getString("phone", null);
        number = preferenceshandyman.getString("Number", null);
        databaseReferencehandyman.child("0"+number).child("Orders").child(PhoneNo).child("Status").setValue("Pending");

        databaseReferenceuser.child(PhoneNo).child("Orders").child("0"+number).child("Status").setValue("Pending");
        System.out.println(number + " helloSS   " + PhoneNo);
        Log.wtf("TAG Hun Mein", number + "    " + PhoneNo);
        //Long hndID = Long.parseLong(number);
        play = (ImageView) findViewById(R.id.play);
        stop = (ImageView) findViewById(R.id.stop);
        send = (ImageView) findViewById(R.id.send);
        record = (ImageView) findViewById(R.id.record);
        label = (TextView) findViewById(R.id.status);

        play.setEnabled(false);
        send.setEnabled(false);
        stop.setEnabled(false);


        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        myAudioRecorder.setOutputFile(outputFile);

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    myAudioRecorder.prepare();
                    myAudioRecorder.start();
                } catch (IllegalStateException ise) {
                    // make something ...
                } catch (IOException ioe) {
                    Log.e(LOG_TAG, "prepare failed");
                    // make something
                }
                label.setText("Recording Audio . . .");
                record.setEnabled(false);
                stop.setEnabled(true);
                Toast.makeText(getApplicationContext(), "Recording started", Toast.LENGTH_LONG).show();
            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                label.setText("Audio Recording Stopped ");
                myAudioRecorder.stop();
                myAudioRecorder.release();
                myAudioRecorder = null;
                record.setEnabled(true);
                stop.setEnabled(false);
                play.setEnabled(true);
                send.setEnabled(true);
                Toast.makeText(getApplicationContext(), "Audio Recorder stopped", Toast.LENGTH_LONG).show();
            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(outputFile);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    label.setText("Audio Recording Playing . . . ");
                } catch (Exception e) {
                    // make something
                }
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                label.setText("Sending Audio Recording");

                Toast.makeText(getApplicationContext(), "Sending Audio", Toast.LENGTH_LONG).show();
                uploadAudio();


            }
        });


    }
    private void uploadAudio()
    {
        progressDialog.setMessage("Uploading Audio...");
        progressDialog.show();
        Log.i("TAG Hun Mein", number + PhoneNo);
        StorageReference filepath = storageRef.child("Audio/0"+number).child(PhoneNo+".mp3");

        Uri uri = Uri.fromFile(new File(outputFile));
        filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                progressDialog.dismiss();
                Intent intent = new Intent(AudioRecorder.this, MyOrders.class);
                startActivity(intent);

            }
        });

    }

}
