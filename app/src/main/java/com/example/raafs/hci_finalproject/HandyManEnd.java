package com.example.raafs.hci_finalproject;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;

import java.io.File;

public class HandyManEnd extends AppCompatActivity {



    FirebaseStorage storage = FirebaseStorage.getInstance();



    Boolean Success;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_handy_man_end);


        Button play= findViewById(R.id.play);


        Button accept = findViewById(R.id.accept);
        Button cancel= findViewById(R.id.cancel);




        Intent intent = getIntent();


        final String PhoneNumber = intent.getStringExtra("UserPhoneNumber");


        final String HandyPhoneNumber = intent.getStringExtra("HandyManNumber");


        final String BROADCAST = "PACKAGE_NAME.android.action.broadcast";




       File path= Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_MUSIC+"/"+PhoneNumber+".mp3");



        final String Path = path.getAbsolutePath();


        final String[] localFile = {null};



        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                DatabaseReference myRef = database.getReference();

                DatabaseReference myRef2;

                DatabaseReference myRef3;



                myRef2=myRef.child("Handyman").child(HandyPhoneNumber).child("Orders").child(PhoneNumber).child("Status");
                myRef2.setValue("Pending");




                myRef3=myRef.child("Users").child(PhoneNumber).child("Orders").child(HandyPhoneNumber).child("Status");

                myRef3.setValue("Pending");

                Intent i = new Intent ( HandyManEnd.this ,  StartJobs.class );
                Bundle b = new Bundle ( );b.putString ("userType" , "REPAIRPERSON" ) ;
                i.putExtras(b);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
            }
        });





        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ContextCompat.checkSelfPermission(
                        HandyManEnd.this,android.Manifest.permission.CALL_PHONE) !=
                        PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions((Activity) HandyManEnd.this, new
                            String[]{android.Manifest.permission.CALL_PHONE}, 0);
                } else {

                    CallReciver.phonenumber=PhoneNumber;
                    CallReciver.Handyphonenumber=HandyPhoneNumber;
                    Intent i = new Intent ( HandyManEnd.this ,  StartJobs.class );
                    Bundle b = new Bundle ( );b.putString ("userType" , "REPAIRPERSON" ) ;
                    i.putExtras(b);
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);

                    startActivity(new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + PhoneNumber)));
                }




            }
        });




        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    Log.v("OK",Path);
                    mediaPlayer.setDataSource(Path);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                } catch (Exception e) {
                    // make something
                }




            }
        });



    }






}
