package com.example.raafs.hci_finalproject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by asim_ on 5/21/2018.
 */

public class CompletedOrders extends Fragment
{
    private LinearLayout mLinearLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_myorder_completed, container, false);
        mLinearLayout = (LinearLayout) rootView.findViewById (R.id.completed_linear_layout);

        SharedPreferences preferencesusers = rootView.getContext().getSharedPreferences("userDetails", Context.MODE_PRIVATE);
        final String user_number = preferencesusers.getString("phone", null);
        // rating = preferenceshandyman.getFloat("Rating", (float)0.0);
        FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
        final DatabaseReference databaseReference =   mFirebaseDatabase.getReference().child("Handyman");
        //  final DatabaseReference databaseReferenceHandyman = mFirebaseDatabase.getReference().child("Handyman");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot childDataSnapshot : dataSnapshot.getChildren()) {
                    Log.v("TAG 2",""+ childDataSnapshot.getKey()); //displays the key for the node
                    Log.v("TAG 1",""+ childDataSnapshot.child("Name").getValue());   //gives the value for given keyname
                    //String tag = String.valueOf(childDataSnapshot.child("Tag").getValue());

                    String status = String.valueOf(childDataSnapshot.child("Orders")
                            .child(user_number).child("Status").getValue());


                    if(status.equals("Completed"))
                    {
                        Log.wtf("TAG Me",  String.valueOf(childDataSnapshot.child("Status").getValue()));
/*

                            Long phone = Long.parseLong(handy_number);*/
                        addLayout ( String.valueOf(childDataSnapshot.child("Name").getValue())
                                , String.valueOf(childDataSnapshot.child("Location").getValue()),
                                Float.parseFloat(String.valueOf(childDataSnapshot.child("Rating").getValue())),
                                Long.parseLong(String.valueOf(childDataSnapshot.getKey()) ), rootView);



                    }
//                        Float rating = Float.parseFloat(review);




/*
                    Log.v("TAG 2",""+ childDataSnapshot.getKey()); //displays the key for the node
                    Log.v("TAG 1",""+ childDataSnapshot.child("Name").getValue());   //gives the value for given keyname
                    String tag = String.valueOf(childDataSnapshot.child("Tag").getValue());
                    tag = tag + "s";
                    Log.v("Equality", tag + " ==== " + data);
                    if(data.equals(tag))
                    {

                        String review = childDataSnapshot.child("Rating").getValue().toString();
                        String number = childDataSnapshot.child("Number").getValue().toString();
                        Long phone = Long.parseLong(number);
                        Float rating = Float.parseFloat(review);
                        addLayout ( childDataSnapshot.child("Name").getValue().toString()
                                , childDataSnapshot.child("Location").getValue().toString()
                                , rating , phone , data);

                    }
*/
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
     /*   addLayout ( "Shahzada Saleem" , "Mochi Gate" , 2.50f , 2 ) ;
        addLayout ( "Anarkali" , "Anarkali" , 5.0f , 3 ) ;
        addLayout ( "Badshah Akbar" , "Akbari Gate" , 1.50f , 1 ) ;
        addLayout ( "Shahzada Saleem" , "Mochi Gate" , 2.50f , 2 ) ;
        addLayout ( "Anarkali" , "Anarkali" , 5.0f , 3 ) ;
        addLayout ( "Badshah Akbar" , "Akbari Gate" , 1.50f , 1 ) ;
        addLayout ( "Shahzada Saleem" , "Mochi Gate" , 2.50f , 2 ) ;
        addLayout ( "Anarkali" , "Anarkali" , 5.0f , 3 ) ;
*/
        return rootView ;
    }


    private void addLayout ( String name , String location , float rating , final Long hndID, final View rootView)
    {
//        View hndBtn = LayoutInflater.from(this).inflate ( R.layout.handy_button, mLinearLayout, false );
        View hndBtn = LayoutInflater.from(getContext()).inflate ( R.layout.tile_completed, mLinearLayout, false );
        TextView tvName = (TextView) hndBtn.findViewById ( R.id.hbtn_Name);
        TextView tvLoc = (TextView) hndBtn.findViewById ( R.id.hbtn_Location);
        RatingBar rbRating = (RatingBar) hndBtn.findViewById ( R.id.hbtn_rating);
        hndBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
//                Toast.makeText(getApplicationContext(), "" + hndID , Toast.LENGTH_SHORT ).show();
                Toast.makeText(getContext(), "" + hndID , Toast.LENGTH_SHORT ).show();
            }
        });
        Button btnIDtoW = (Button) hndBtn.findViewById ( R.id.btnIDtoW);
        btnIDtoW.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(rootView.getContext(), HomeScreen.class);
                startActivity(intent);

            }
        });

        tvName.setText ( name );
        tvLoc.setText ( location ) ;
        rbRating.setRating ( rating ) ;

        mLinearLayout.addView ( hndBtn ) ;

    }
}
