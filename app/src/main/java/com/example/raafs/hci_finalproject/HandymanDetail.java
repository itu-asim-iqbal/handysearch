package com.example.raafs.hci_finalproject;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.io.IOException;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.MODE_WORLD_READABLE;
import static com.google.android.gms.internal.zzahn.runOnUiThread;

public class HandymanDetail extends Fragment
{
    ImageView recorder ;

    //-------------------------------ALERT DIALOG VARS
    ImageView play, stop, send, record;
    TextView label;

    MediaRecorder myAudioRecorder = new MediaRecorder();

    final String outputFile = Environment.getExternalStorageDirectory().getAbsolutePath() + "/job.3gp";
    ProgressDialog progressDialog;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReference();
    final String LOG_TAG = " Record Log";


    String PhoneNo;
    String number;
    //-------------------------------ALERT DIALOG VARS

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        final View rootView = inflater.inflate(R.layout.fragment_handyman_info_details, container, false);
        recorder = (ImageView) rootView.findViewById(R.id.recording);
        TextView skills;
        RatingBar ratingBar;

        skills = (TextView) rootView.findViewById(R.id.skill);
        ratingBar = (RatingBar) rootView.findViewById(R.id.ratingBar);

        SharedPreferences preferences = rootView.getContext().getSharedPreferences( "handymanDetails", MODE_PRIVATE ) ;

         String handymanSkill = preferences.getString("Skill",null );
        Float ratingStars =  preferences.getFloat("Rating", (float) 0.0);


        Log.wtf("TAG", handymanSkill);
        skills.setText(handymanSkill);
        ratingBar.setRating(ratingStars);
        recorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText( rootView.getContext(), "Incread Media Volumn", Toast.LENGTH_LONG);
//                Intent intent = new Intent(rootView.getContext(), AudioRecorder.class);
//                startActivity(intent);

                recordingDialogOpener(rootView);
            }
        });
        return rootView;
    }

    public void recordingDialogOpener ( final View rootView )
    {
        LayoutInflater li = LayoutInflater.from(rootView.getContext());
        View promptsView = li.inflate ( R.layout.record_dialog, null);
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder ( rootView.getContext() ) ;
        alertDialogBuilder.setView ( promptsView ) ;



        progressDialog = new ProgressDialog(rootView.getContext());
        final MediaPlayer mp = MediaPlayer.create(rootView.getContext(), R.raw.handysearch);
        mp.start();

        FirebaseDatabase mFirebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference databaseReferencehandyman =    mFirebaseDatabase.getReference().child("Handyman");
        DatabaseReference databaseReferenceuser =    mFirebaseDatabase.getReference().child("Users");


        SharedPreferences preferencesuser, preferenceshandyman;
        preferencesuser = rootView.getContext().getSharedPreferences( "userDetails", MODE_PRIVATE ) ;
        preferenceshandyman = rootView.getContext().getSharedPreferences( "handymanDetails", MODE_PRIVATE ) ;
        PhoneNo = preferencesuser.getString("phone", null);
        number = preferenceshandyman.getString("Number", null);
        databaseReferencehandyman.child("0"+number).child("Orders").child(PhoneNo).child("Status").setValue("Pending");

        databaseReferenceuser.child(PhoneNo).child("Orders").child("0"+number).child("Status").setValue("Pending");
        System.out.println(number + " helloSS   " + PhoneNo);
        Log.wtf("TAG Hun Mein", number + "    " + PhoneNo);
        //Long hndID = Long.parseLong(number);
        play = (ImageView) promptsView.findViewById(R.id.play);
        stop = (ImageView) promptsView.findViewById(R.id.stop);
        send = (ImageView) promptsView.findViewById(R.id.send);
         record = (ImageView) promptsView.findViewById(R.id.record);
        label = (TextView) promptsView.findViewById(R.id.status);


        alertDialogBuilder.setCancelable(false) ;
        final AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                label.setText("Please record your message purely in Urdu. Message should include your name, location and job description");

                play.setEnabled(false);
                send.setEnabled(false);
                stop.setEnabled(false);

            }
        });


        myAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        myAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        myAudioRecorder.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB);
        myAudioRecorder.setOutputFile(outputFile);

        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    myAudioRecorder.prepare();
                    myAudioRecorder.start();
                } catch (IllegalStateException ise) {
                    // make something ...
                } catch (IOException ioe) {
                    Log.e(LOG_TAG, "prepare failed");
                    // make something
                }
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        label.setText("Recording Audio . . .");
                        record.setEnabled(false);
                        stop.setEnabled(true);
                        Toast.makeText(rootView.getContext(), "Recording started", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAudioRecorder.stop();
                myAudioRecorder.release();
                myAudioRecorder = null;
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        label.setText("Audio Recording Stopped ");
                        record.setEnabled(true);
                        stop.setEnabled(false);
                        play.setEnabled(true);
                        send.setEnabled(true);
                        Toast.makeText(rootView.getContext(), "Audio Recorder stopped", Toast.LENGTH_LONG).show();
                    }
                });

            }
        });

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MediaPlayer mediaPlayer = new MediaPlayer();
                try {
                    mediaPlayer.setDataSource(outputFile);
                    mediaPlayer.prepare();
                    mediaPlayer.start();
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            label.setText("Audio Recording Playing . . . ");
                        }
                    });
                } catch (Exception e) {
                    // make something
                }
            }
        });
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        label.setText("Sending Audio Recording");
                        alertDialog.dismiss();
                        Toast.makeText(rootView.getContext(), "Sending Audio", Toast.LENGTH_LONG).show();
                    }
                });
                uploadAudio(rootView);
            }
        });


    }

    private void uploadAudio(final View rootView)
    {
        progressDialog.setMessage("Uploading Audio...");
        progressDialog.show();
        Log.i("TAG Hun Mein", number + PhoneNo);
        StorageReference filepath = storageRef.child("Audio/0"+number).child(PhoneNo+".mp3");

        Uri uri = Uri.fromFile(new File(outputFile));
        filepath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                progressDialog.dismiss();
                Intent intent = new Intent(rootView.getContext(), MyOrders.class);
                startActivity(intent);
                getActivity().finish();

            }
        });

    }
}
